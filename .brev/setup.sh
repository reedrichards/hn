#!/usr/bin/env zsh

set -euo pipefail

echo "begin setup"
echo `whoami`
echo `pwd`
echo `env`
#  hack to workaround homebrew bug
sudo chown -R brev /home/linuxbrew
echo 'eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"' >> /home/brev/.zshrc
eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"
brew install pyenv
echo 'export PYENV_ROOT="$HOME/.pyenv"' >> ~/.zshrc
echo 'eval "$(pyenv init -)"' >> ~/.zshrc
export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"    # if `pyenv` is not already on PATH
eval "$(pyenv init --path)"
eval "$(pyenv init -)"
pyenv install -s 3.7.5

sudo apt update && sudo apt install -y build-essential python3.8-venv python3-pip python3-distutils python3-apt
echo "export PATH=/home/brev/.local/bin:\$PATH" >> ~/.zshrc
export PATH=/home/brev/.local/bin:$PATH
pip3 install virtualenv
pip3 install virtualenvwrapper

sed -e '/plugins/ s/^#*/#/' -i ~/.zshrc
sed -e '/source/ s/^#*/#/' -i ~/.zshrc
echo "plugins=(git virtualenvwrapper)" >> ~/.zshrc
echo "source \$ZSH/oh-my-zsh.sh" >> ~/.zshrc
zsh -ic 'mkvirtualenv --python=/home/brev/.pyenv/versions/3.7.5/bin/python3.7 -a /home/brev/workspace/hn hn'
