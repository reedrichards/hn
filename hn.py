#!/usr/bin/env nix-shell
#! nix-shell -i python -p python3 python3Packages.irc 
from html.parser import HTMLParser
from typing import Set
from urllib.request import urlopen
from urllib.error import HTTPError
from socket import timeout
from xmlrpc.client import Boolean
from time import sleep
import socket
import irc.client
import time


# https://stackoverflow.com/questions/17336943/removing-non-numeric-characters-from-a-string
def only_nums_from_string(your_string):
    return "".join(ch for ch in your_string if ch.isdigit())


class FrontPageParser(HTMLParser):
    item_ids: Set[str] = set()

    def handle_starttag(self, tag, attrs):
        attr_dict = dict(attrs)
        if tag == "tr" and attr_dict.get("class") == "\\'athing\\'":
            self.item_ids.add(attr_dict.get("id", ""))

    def get_cleanded_item_ids(self):
        return [only_nums_from_string(item_id) for item_id in self.item_ids]


class ItemPageParser(HTMLParser):
    print_data: Boolean = False
    link: str = ""
    link_title: str = ""

    def __repr__(self) -> str:
        return f"link: {self.link} title: {self.link_title}"

    def handle_starttag(self, tag, attrs):
        attr_dict = dict(attrs)
        if 'class' in attr_dict and attr_dict['class'] == "titleline":
            self.print_data = True

    def handle_data(self, data) -> None:
        if self.print_data:
            self.link_title = data
            self.print_data = False


def fetch_story_link_and_title_from_hn_link(link):
    try:
        html_string = str(urlopen(link, timeout=10).read())

        parser = ItemPageParser()
        parser.feed(html_string)
        title_link = f"""
{parser.link_title}
{parser.link}
{link}
"""

    except HTTPError as e:
        if e.code == 503:
            sleep(5)

            html_string = str(urlopen(link, timeout=10).read())
            parser = ItemPageParser()
            parser.feed(html_string)
            title_link = f"""
{parser.link_title}
{parser.link}
{link}
    """
        else:
            title_link = f"{e.code} {link}"
    except timeout:
        title_link = f"timeout {link}"
    return title_link


def prefix_id_with_link(item_id):
    return "https://news.ycombinator.com/item?id=" + item_id


# IRC server details
server = "100.71.200.14"  # Replace with your IRC server
port = 6667  # Default IRC port
nickname = "news"  # Replace with your desired nickname



# adding a bunch of import statements and logic here so that when
# FrontPageParser is imported as a library, all of this is not run,
# which should keep memory usage down.
if __name__ == "__main__":
    import sys
    import urllib.request
    from datetime import date, timedelta
    import os
    import argparse


    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument("-t", "--token", help="slack bot token", type=str)
    arg_parser.add_argument("-c", "--channel", help="channel id", type=str)
    # add --msg to send a message to the channel
    arg_parser.add_argument("--msg", help="message to send to the channel", type=str)

    parsed_args = arg_parser.parse_args(sys.argv[1:])

    if parsed_args.channel:
        CHANNEL_ID = parsed_args.channel
    else:
        CHANNEL_ID = os.environ.get("SLACK_CHANNEL_ID")

    if parsed_args.token:
        BOT_TOKEN = parsed_args.token
    else:
        BOT_TOKEN = os.environ.get("SLACK_BOT_TOKEN")

    if parsed_args.msg:
        msg = parsed_args.msg
    else:
        yesterday = date.today() - timedelta(days=1)
        yesterday.strftime("%Y-%m-%d")
        frontpage_parser = FrontPageParser()
        with urllib.request.urlopen(
            f"https://news.ycombinator.com/front?day={yesterday}"
        ) as response:
            yesterday_front_page_html = str(response.read())
        frontpage_parser.feed(yesterday_front_page_html)
        msg = "\n".join(
            (
                fetch_story_link_and_title_from_hn_link(prefix_id_with_link(item_id))
                for item_id in frontpage_parser.get_cleanded_item_ids()
            )
        )
    print(msg)
    msg = msg.replace("\\n", "\n")

        # Connect to the IRC server
    client = irc.client.Reactor()

    def on_connect(connection, event):
        connection.join("#news")
        connection.privmsg("#news", msg)
        connection.quit("Bye!")

    c = client.server().connect(server, port, nickname)
    c.add_global_handler("welcome", on_connect)
    while True:
        client.process_once(timeout=0.2)
        if not c.connected:
            break

    # exit after message is sent