#!/usr/bin/env python3

# https://lobste.rs/top/1d

from html.parser import HTMLParser
from dataclasses import dataclass

@dataclass
class Lobster:
    title: str
    link: str
    comment_title: str
    comment_link: str

    @property
    def str(self) -> str:
        return f"""{self.title} 
{self.link} 
{self.get_comment_slack_link()}
"""

    def __repr__(self) -> str:
        return self.str

    def __str__(self) -> str:
        return self.str

    def get_comment_title(self) -> str:
        return self.comment_title.replace("\\n", "").strip()
    
    def get_comment_link(self) -> str:
        return "https://lobste.rs" + self.comment_link

    def get_comment_slack_link(self) -> str:
        # <https://zapier.com|Link text>
        return f"<{self.get_comment_link()}|{self.get_comment_title()}>"


class LobstersParser(HTMLParser):
    # <a class="u-url" href="https://github.com/danielfullmer/robotnix"
    # rel="ugc noreferrer">robotnix:
    # Build Android (AOSP) using Nix</a>
    # extract the link and title from the above html
    links = []
    titles = []
    add_data = False
    def handle_starttag(self, tag: str, attrs) -> None:
        attr_dict = dict(attrs)
        if tag == "a" and "class" in attr_dict and attr_dict["class"] == "u-url":
            self.links.append(attr_dict["href"])
            self.add_data = True
        
    def handle_data(self, data: str) -> None:
        if self.add_data:
            self.titles.append(data)
            self.add_data = False

class CommentParser(HTMLParser):
# <a role="heading" aria-level="2" href="/s/wgkc2x/algorithm_source_code_for_twitter_s">
#     13 comments</a>
    links = []
    titles = []
    add_data = False
    def handle_starttag(self, tag: str, attrs) -> None:
        attr_dict = dict(attrs)
        if tag == "a" and "role" in attr_dict and attr_dict["role"] == "heading":
            self.links.append(attr_dict["href"])
            self.add_data = True
    
    def handle_data(self, data: str) -> None:
        if self.add_data:
            self.titles.append(data)
            self.add_data = False



if __name__ == "__main__":
    import sys
    import urllib.request
    import os
    import argparse

    from slack_sdk import WebClient

    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument("-t", "--token", help="slack bot token", type=str)
    arg_parser.add_argument("-c", "--channel", help="slack channel id", type=str)
    parsed_args = arg_parser.parse_args(sys.argv[1:])

    if parsed_args.channel:
        CHANNEL_ID = parsed_args.channel
    else:
        CHANNEL_ID = os.environ.get("SLACK_CHANNEL_ID")

    if parsed_args.token:
        BOT_TOKEN = parsed_args.token
    else:
        BOT_TOKEN = os.environ.get("SLACK_BOT_TOKEN")

    client = WebClient(token=BOT_TOKEN)

    url = "https://lobste.rs/top/1d"
    html_string = str(urllib.request.urlopen(url, timeout=10).read())
    parser = LobstersParser()
    parser.feed(html_string)
    comment_parser = CommentParser()
    comment_parser.feed(html_string)

    lobsters_articles = []
    for title, link, comment_title, comment_link in zip(parser.titles, parser.links, comment_parser.titles, comment_parser.links):
        lobsters_articles.append(Lobster(title, link, comment_title, comment_link))
    slack_msg = "\n\n".join([str(lobster) for lobster in lobsters_articles])
    client.chat_postMessage(channel=CHANNEL_ID, text=slack_msg)
