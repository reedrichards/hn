Get yesterday's top posts of hackernews and post the url's title and link to
a given slack channel.

# Development

## Install Dependencies

### Ubuntu

install python3

```
sudo apt install python3 python3-pip
```

install the slack sdk

```
pip3 install slack_sdk
```

configure environment variables
```
export SLACK_CHANNEL_ID=
export SLACK_BOT_TOKEN=
```

or pass them as command line arguements

```shell
python3 hn.py --token xoxb-lkfjsdfij34no --channel C0VDCBSFJ3
```

# Design Notes

the entries for articles are formatted like so

```html

<tr class="athing" id="31468303">
  <td align="right" valign="top" class="title">
	<span class="rank">1.
	</span>
  </td>
  <td valign="top" class="votelinks">
	<center>
	  <a id="up_31468303" class="clicky" href="vote?id=31468303&amp;how=up&amp;auth=46da572cb3b6cb588e299bba1802471f2312d922&amp;goto=front%3Fday%3D2022-05-22">
		<div class="votearrow" title="upvote">
		</div>
	  </a>
	</center>
  </td>
  <td class="title">
	<a href="https://avc.com/2022/05/how-this-ends-2/" class="titlelink">How This Ends
	</a>
	<span class="sitebit comhead"> (
	  <a href="from?site=avc.com">
		<span class="sitestr">avc.com
		</span>
	  </a>)
	</span>
  </td>
</tr>

```

and the comments page for an article is formatted like this

```
https://news.ycombinator.com/item?id=31495049
```
and the link and editorialized title for the post for the comments page can be found in the element

```
<a href="https://github.com/dzhang314/YouTubeDrive" class="titlelink">YouTubeDrive: Store files as YouTube videos</a>

```
